const express = require('express')
const app = express()
const redis = require("redis");
const client = redis.createClient();

app.use(express.json())
app.use(express.urlencoded({extended:true}))

client.on('connect', () => {
  client.set('key','value', redis.print)  
  console.log('redis connected')
})

// client.set("key", "value", redis.print);
// client.get("key", redis.print);

app.get('/api', function(req, res, next) {
    let id = req.query.id
    res.send({ id: id });
});

app.post('/api/add', function(req, res) {
    const id = req.body.id
    const username = req.body.username;
    const email = req.body.email;
    const pass = req.body.pass;


    client.HMSET(id, [
      'username', username,
      'email', email,
      'pass', pass
    ], function(err, result){
      if(err){
        console.log(err);
      } else {
        console.log(result);
      }
      res.send('Added...');
  });
});

app.get('/api/search', (req, res) => {
    let id = req.query.id
    
    client.HGETALL(id, (err, result) => {
      if (!result) {
        res.send({error: err})
      } else {
        res.send({result: {id, info: result}})
      }
    })
    
})

app.delete('/api/delete', (req, res) => {
   let id = req.query.id

   client.DEL(id, (err, result) => {
      if (!result) {
          res.send({error: err})
      } else {
          res.send({msg: `user ~${id}~ deleted`})
      }
   })
})

app.listen(3000, () => {
    console.log('server running on port 3000')
})